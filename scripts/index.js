// Теоретичні питання

//     1. Описати своїми словами навіщо потрібні функції у програмуванні.

//         Функції у програмуванні необхідні для створення певної логіки коду, що буде використовуватися декілька разів в інших місцях коду для обробки даних. 

//     2. Описати своїми словами, навіщо у функцію передавати аргумент.

//         Аргумент передається у функцію для забезпечення виконання покладених на функцію завдань, для взаємодії функції з іншим кодом, підвищення її використовуваності для всяких завдань.

//     3. Що таке оператор return та як він працює всередині функції?

//         Оператор return завершує роботу функції, потім повертає значення, яке йому передається. Значення може бути обчислюваним і повертається в місце визову функції.


// Завдання

let number1Hint = document.querySelector('.number1-hint');
let number2Hint = document.querySelector('.number2-hint');

const errorHandler = (text, place) => {

    if (place === 'number1') {
        number1Hint.innerText = text;
    } else if (place === 'number2') {
        number2Hint.innerText = text;
    };
};


const action = document.querySelector('.input-action');

action.addEventListener('input', e => {
    e.target.value = e.target.value.replace(/[^\/\*\-\+]/, '');
});

const validateNumber = (number1, number2) => {

    let switcher = true;

    if (!number1) {
        errorHandler('Вы не ввели число 1! Повторите ввод', 'number1');

        switcher = false;
    }

    if (!number2) {
        errorHandler('Вы не ввели число 2! Повторите ввод', 'number2');

        switcher = false;
    }

    if (!!number1 && isNaN(Number(number1))) {
        errorHandler('Вы ввели некоректное число 1 повторите ввод!!!', 'number1');

        switcher = false;
    }

    if (!!number2 && isNaN(Number(number2))) {
        errorHandler('Вы ввели некоректное число 2 повторите ввод!!!', 'number2');

        switcher = false;
    }

    return switcher;
};


const getAction = () => {

    const number1 = document.querySelector('.input-num1');
    const number2 = document.querySelector('.input-num2');
    const inputAction = document.querySelector('.input-action');
    const output = document.querySelector('.output');

    number1Hint.innerText = '';
    number2Hint.innerText = '';
    output.innerText = '';

    if (validateNumber(number1.value, number2.value) && inputAction.value !== '') {
        const actions = {
            '+': +number1.value + +number2.value,
            '-': +number1.value - +number2.value,
            '*': +number1.value * +number2.value,
            '/': +number1.value / +number2.value,
        };

        output.innerText = 'Результат: ' + actions[inputAction.value]; 
        console.log('Результат: ' + actions[inputAction.value]);
    }

};

const onSubmit = () => {
    const submitBtn = document.querySelector('.submit');

    submitBtn.addEventListener('click', getAction);

};

onSubmit();